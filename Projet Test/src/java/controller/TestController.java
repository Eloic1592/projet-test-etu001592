/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Annotation.TableAnnotation;
import Annotation.UrlAnnotation;
import GenericDao.ObjectBdd;
import Modele.Employe;
import Modele.Personne;
import connex.Connex;
import java.util.ArrayList;
import java.util.HashMap;
import modelView.ModelView;

@TableAnnotation(nameTable = "TestController")
public class TestController {
 
    @UrlAnnotation.url(name="getNomFromUrl",methode="getNomFromUrl")
    public String getNomFromUrl()
    {
        return "test";
    }        
                                                                                                                                                                                             
    @UrlAnnotation.url(name="listeemploye",methode="listeemploye")
    public ModelView listeemploye() throws Exception{
        HashMap<String,Object> valeur = new HashMap();
        Employe employe=new Employe();
        ArrayList<ObjectBdd> obje = employe.Select(new Connex().getConnection());
        valeur.put("tab",obje);
        ModelView vm = new ModelView(valeur,"liste");
        return vm;
    }
    
    @UrlAnnotation.url(name="listepersonne",methode="listepersonne")
    public ModelView listepersonne(ObjectBdd o,HashMap<String,Object> map) throws Exception{
        HashMap<String,Object> valeur = new HashMap();
        ArrayList<ObjectBdd> obje = o.Select(new Connex().getConnection());
        valeur.put("tab",obje);
        o.saveAll(new Connex().getConnection());
        ModelView vm = new ModelView(map,"accueil");
        return vm;
    }
    
    
    @UrlAnnotation.url(name="insert",methode="insert")
    public ModelView insert(ObjectBdd o,HashMap<String,Object> map) throws Exception{
        o.saveAll(new Connex().getConnection());
        ModelView vm = new ModelView(map,"accueil");
        return vm;
    }
}
