

create table Personne(
id serial,
nom varchar(20)
);

insert into Personne(nom) values ('hardi') , ('tojoniaina');


create table Employe(
id serial,
idPers int,
montant double precision 
);

insert into Employe(idPers,montant) values (1,2300.0) , (2,3400.0);

create table Test(
idDepartement int,
nomDepartement varchar(20)
);